//
//  AddViewController.swift
//  TalkBoy
//
//  Created by Cohen, Dor on 23/09/2020.
//

import UIKit

class AddViewController: UIViewController {

    @IBOutlet weak var recordingTitle: UITextField!
    @IBAction func recordAudio(_ sender: Any) {
    }
    @IBAction func playAudio(_ sender: Any) {
    }
    @IBAction func saveAudio(_ sender: Any) {
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }

}
